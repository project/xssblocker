<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function xssblocker_settings_form($form, &$form_state) {
  $form['xssblocker_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the XSS blocker'),
    '#default_value' => variable_get('xssblocker_enabled', 0),
  );
  $form['xssblocker_text_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable text to be displayed after deflecting an attack.'),
    '#default_value' => variable_get('xssblocker_text_enabled', 0),
  );
  $form['xssblocker_text'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#description' => t(''),
    '#default_value' => variable_get('xssblocker_text_enabled', 'Requested page could not be found.'),
  );

  return system_settings_form($form);
}
