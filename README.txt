Vulnerability Blocker
=====================

-- SUMMARY --

The Vulnerability Blocker module stops reflected XSS attacks from occurring on a website.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

* You likely want to disable Toolbar module, since its output clashes with
  Administration menu.


-- CONFIGURATION --

1. Go to admin/config/xssblocker/settings
2. Enable the XSS blocker
